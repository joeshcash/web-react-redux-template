import { useEffect } from "react";
import { Switch, Route } from "react-router-dom";
import { connect } from "react-redux";

import routes from "./config/routes";

import { getUsersAsync } from "./store/reducers/example/example.actions";

import HomePage from "./pages/home/home.page";
import NotFoundPage from "./pages/not-found/NotFound.page";

import GlobalStyle from "./theme/GlobalStyle.styles";

const App = ({ getUsersAsync }) => {
  useEffect(() => {
    getUsersAsync();
  }, []);

  return (
    <>
      <GlobalStyle />
      <Switch>
        <Route path={routes.root} exact component={HomePage} />
        <Route component={NotFoundPage} />
      </Switch>
    </>
  );
};

const mapDispatchToProps = (dispatch) => ({
  getUsersAsync: () => dispatch(getUsersAsync()),
});

export default connect(null, mapDispatchToProps)(App);
