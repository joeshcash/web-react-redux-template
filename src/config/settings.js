const settings = {
  baseUrl: process.env.REACT_APP_BASE_URL,
};

export default settings;
