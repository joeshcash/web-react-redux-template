import API from "../helpers/API";
import { isDev, sleep } from "../helpers/devTools";

import { getUsers } from "../store/reducers/example/example.actions";

export class UserService {
  static getUsersAsync(uri, dispatch) {
    return new Promise((resolve, reject) => {
      API.get(uri)
        .then((res) => {
          const data = res.data;

          sleep(3000).then(() => {
            dispatch(getUsers(data));
            resolve();
          });
        })
        .catch((error) => {
          if (isDev(error)) {
            console.log(error);
          }

          reject(error);
        });
    });
  }
}
