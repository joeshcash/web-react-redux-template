import { createSelector } from "reselect";

export const selectExample = (state) => state.example;

export const selectExampleUsers = createSelector(
  [selectExample],
  (example) => example.users
);

export const selectExampleLoading = createSelector(
  [selectExample],
  (example) => example.loading
);
