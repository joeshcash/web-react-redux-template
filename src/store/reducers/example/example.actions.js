import exampleTypes from "./example.types";

import { UserService } from "../../../services/user.service";

export const getUsers = (users) => ({
  type: exampleTypes.GET_USERS,
  payload: users,
});

export const setLoadingUsers = () => ({
  type: exampleTypes.SET_LOADING,
});

export const getUsersAsync = () => async (dispatch) => {
  const ressource = "/users";

  dispatch(setLoadingUsers());

  return await UserService.getUsersAsync(ressource, dispatch);
};
