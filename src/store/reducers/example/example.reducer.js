import exampleTypes from "./example.types";

const INITIAL_STATE = {
  users: [],
  loading: false,
};

const exampleReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case exampleTypes.GET_USERS:
      return {
        ...state,
        users: action.payload,
        loading: false,
      };

    case exampleTypes.SET_LOADING:
      return {
        ...state,
        loading: true,
      };

    default:
      return state;
  }
};

export default exampleReducer;
