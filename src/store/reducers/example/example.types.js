const exampleTypes = {
  GET_USERS: "GET_USERS",
  SET_LOADING: "SET_LOADING",
};

export default exampleTypes;
