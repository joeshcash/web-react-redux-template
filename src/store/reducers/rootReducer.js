import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

import exampleReducer from "./example/example.reducer";

export const persistConfig = {
  key: "root",
  storage,
  whitelist: [],
  blacklist: [],
};

const topReducer = combineReducers({
  example: exampleReducer,
});

const rootReducer = (state, action) => {
  // when RESET action is dispatched it will reset redux state
  //   if (action.type === RESET) {
  //     state = undefined;
  //   }

  return topReducer(state, action);
};

export default persistReducer(persistConfig, rootReducer);
