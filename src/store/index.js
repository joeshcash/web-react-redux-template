import { createStore, applyMiddleware } from "redux";
import { persistStore } from "redux-persist";
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";

import { isDev } from "../helpers/devTools";

import rootReducer from "./reducers/rootReducer";

const middlewares = [
  thunk,
  createLogger({
    predicate: () => isDev(),
    collapsed: true,
  }),
];

export const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(...middlewares))
);

export const persistor = persistStore(store);
