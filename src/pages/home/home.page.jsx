import React from "react";

import { UsersContainer as UsersLayout } from "../../components/Layouts/Users/Users.container";

import "./home.styles.scss";

const HomePage = () => {
  return (
    <div className="home-page">
      <UsersLayout />
    </div>
  );
};

export default HomePage;
