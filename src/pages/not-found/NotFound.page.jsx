import React from "react";
import { Link } from "react-router-dom";
import routes from "../../config/routes";

import "./NotFound.styles.scss";

const NotFoundPage = () => {
  return (
    <div className="not-found-page">
      <h1>Page Introuvable</h1>

      <Link to={routes.root}>Revenir à l'accueil</Link>
    </div>
  );
};

export default NotFoundPage;
