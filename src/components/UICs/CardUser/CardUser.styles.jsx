import styled from "styled-components";

export const CardContainer = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid #ddd;
  border-radius: 7px;
  margin-bottom: 15px;
`;

export const CardHeader = styled.div`
  padding: 5px 15px;
  font-size: 16px;
  font-weight: bold;
  color: #141414;
`;

export const CardBody = styled.div`
  padding: 5px 15px;

  p {
    margin-bottom: 5px;
    color: #333;
  }
`;
