import React from "react";

import { CardBody, CardContainer, CardHeader } from "./CardUser.styles";

const CardUser = ({ name, email, website }) => {
  return (
    <CardContainer>
      <CardHeader>{name}</CardHeader>
      <CardBody>
        <p>Email: {email}</p>
        <p>Website: {website}</p>
      </CardBody>
    </CardContainer>
  );
};

export default CardUser;
