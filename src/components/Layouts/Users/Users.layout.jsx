import React from "react";

import ListLoaderLoader from "../../Loaders/ListLoader.loader";

import CardUser from "../../UICs/CardUser/CardUser.uic";

import "./Users.styles.scss";

const UsersLayout = ({ users, loading }) => {
  return (
    <div className="l-users">
      {loading ? (
        <ListLoaderLoader />
      ) : (
        users.map((user) => <CardUser key={user.id} {...user} />)
      )}
    </div>
  );
};

export default UsersLayout;
