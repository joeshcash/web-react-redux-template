import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

import {
  selectExampleLoading,
  selectExampleUsers,
} from "../../../store/reducers/example/example.selectors";

import UsersLayout from "./Users.layout";

const mapStateToProps = createStructuredSelector({
  users: selectExampleUsers,
  loading: selectExampleLoading,
});

export const UsersContainer = connect(mapStateToProps)(UsersLayout);
