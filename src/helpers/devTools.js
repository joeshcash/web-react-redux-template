export const sleep = (time) => new Promise((acc) => setTimeout(acc, time));

export const isDev = () => {
  return !process.env.NODE_ENV || process.env.NODE_ENV === "development";
};
