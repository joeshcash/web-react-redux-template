import axios from "axios";

import settings from "../config/settings";

const API = axios.create({
  baseURL: settings.baseUrl,
});

export default API;
